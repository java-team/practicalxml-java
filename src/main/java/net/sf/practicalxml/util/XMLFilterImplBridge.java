// Copyright 2008-2014 severally by the contributors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package net.sf.practicalxml.util;

import java.io.IOException;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;
import org.xml.sax.helpers.XMLFilterImpl;


/**
 *  This class acts as a bridge between the <code>DefaultHandler</code>
 *  expected by <code>SAXParser</code>, and the <code>XMLReader</code>
 *  used by <code>SAXSource</code>. The latter is normally implemented
 *  using {@link org.xml.sax.helpers.XMLFilterImpl}, and this class is
 *  a wrapper for that class that simply passes all events.
 *  <p>
 *  For example usage, see {@link SimpleXMLReader}.
 *
 *  @since 1.1.1
 */
public class XMLFilterImplBridge
extends DefaultHandler
{
    private XMLFilterImpl _filter;

    public XMLFilterImplBridge(XMLFilterImpl filter)
    {
        _filter = filter;
    }

    @Override
    public void characters(char[] ch, int start, int length)
    throws SAXException
    {
        _filter.characters(ch, start, length);
    }

    @Override
    public void endDocument()
    throws SAXException
    {
        _filter.endDocument();
    }

    @Override
    public void endElement(String uri, String localName, String name)
    throws SAXException
    {
        _filter.endElement(uri, localName, name);
    }

    @Override
    public void endPrefixMapping(String prefix)
    throws SAXException
    {
        _filter.endPrefixMapping(prefix);
    }

    @Override
    public void error(SAXParseException e)
    throws SAXException
    {
        _filter.error(e);
    }

    @Override
    public void fatalError(SAXParseException e)
    throws SAXException
    {
        _filter.fatalError(e);
    }

    @Override
    public void ignorableWhitespace(char[] ch, int start, int length)
    throws SAXException
    {
        _filter.ignorableWhitespace(ch, start, length);
    }

    @Override
    public void notationDecl(String name, String publicId, String systemId)
    throws SAXException
    {
        _filter.notationDecl(name, publicId, systemId);
    }

    @Override
    public void processingInstruction(String target, String data)
    throws SAXException
    {
        _filter.processingInstruction(target, data);
    }

    @Override
    public InputSource resolveEntity(String publicId, String systemId)
    throws IOException, SAXException
    {
        return _filter.resolveEntity(publicId, systemId);
    }

    @Override
    public void setDocumentLocator(Locator locator)
    {
        _filter.setDocumentLocator(locator);
    }

    @Override
    public void skippedEntity(String name)
    throws SAXException
    {
        _filter.skippedEntity(name);
    }

    @Override
    public void startDocument()
    throws SAXException
    {
        _filter.startDocument();
    }

    @Override
    public void startElement(String uri, String localName, String name, Attributes attributes)
    throws SAXException
    {
        _filter.startElement(uri, localName, name, attributes);
    }

    @Override
    public void startPrefixMapping(String prefix, String uri)
    throws SAXException
    {
        _filter.startPrefixMapping(prefix, uri);
    }

    @Override
    public void unparsedEntityDecl(String name, String publicId, String systemId, String notationName)
    throws SAXException
    {
        _filter.unparsedEntityDecl(name, publicId, systemId, notationName);
    }

    @Override
    public void warning(SAXParseException e)
    throws SAXException
    {
        _filter.warning(e);
    }
}
