// Copyright 2008-2014 severally by the contributors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package net.sf.practicalxml.converter.bean;


/**
 *  Options used by {@link Bean2XmlConverter} to control the structure of the
 *  generated DOM tree.
 *
 *  @since 1.1
 */
public enum Bean2XmlOptions
{
    /**
     *  Outputs byte arrays as a Base64-encoded string
     *
     *  @since 1.1.18
     */
    BYTE_ARRAYS_AS_BASE64,

    /**
     *  Outputs byte arrays as a hex-encoded string
     *
     *  @since 1.1.18
     */
    BYTE_ARRAYS_AS_HEX,

    /**
     *  Will use a shared static introspection cache for all conversions.
     *  <p>
     *  <strong>Warning</strong>: if you use this option, do not store this
     *  library in a shared app-server classpath. If you do, the cache will
     *  prevent class unloading, and you will run out of permgen space.
     */
    CACHE_INTROSPECTIONS,

    /**
     *  Defer exception processing. When an exception occurs during conversion,
     *  the node causing that exception is skipped and the exception is added
     *  to a list. The caller can then examine that list and take appropriate
     *  action.
     *  <p>
     *  Note: all deferred exceptions will be of type
     *  {@link net.sf.practicalxml.converter.ConversionException}, and will wrap
     *  an underlying cause. Because the converter works by reflection, in many
     *  cases this cause will be an <code>InvocationTargetException</code> with
     *  its own cause.
     *
     *  @since 1.1.15
     */
    DEFER_EXCEPTIONS,


    /**
     *  Output enum instances with an attribute containing the enum's name, and
     *  the text value containing the enum's <code>toString()</code>. Default is
     *  to output the enum's name as the element value.
     *  <p>
     *  This option is useful when the enum contains descriptive text, and you
     *  want to use that text in further output.
     *
     *  @since 1.1.17
     */
    ENUM_AS_NAME_AND_VALUE,

    /**
     *  Output maps in an "introspected" format, where the name of each item
     *  is the map key (rather than "data"), and the "key" attribute is omitted.
     *  If any key is not a valid XML identifier, the converter will throw.
     */
    MAP_KEYS_AS_ELEMENT_NAME,

    /**
     *  If the value is <code>null</code>, add an element containing a single
     *  text child holding an empty string.
     *  <p>
     *  This may make life easier when processing data from a certain DBMS
     *  designed in the mid-1980s when disk space was too expensive to create
     *  a separate null flag for VARCHAR fields. However, be aware that it
     *  may cause parsing problems.
     */
    NULL_AS_EMPTY,

    /**
     *  If the value is <code>null</code>, add an element without content, with
     *  the attribute <code>xsi:nil</code> set to "true".
     */
    NULL_AS_XSI_NIL,

    /**
     *  Will create sequences (arrays, lists, and sets) as repeated elements
     *  rather than a parent-children construct. This option is invalid when
     *  converting an array as the top-level object, as it would cause the
     *  creation of multiple root elements. It also produces output that can
     *  not, at this time, be processed correctly by {@link Xml2BeanConverter}.
     */
    SEQUENCE_AS_REPEATED_ELEMENTS,

    /**
     *  When introspecting, call <code>setAccessible(true)</code> on accessor
     *  methods. This allows use with instances of private classes that have
     *  public methods, but will cause an exception in any environment that has
     *  a security manager that denies this operation.
     *
     *  @since 1.1.15
     */
    SET_ACCESSIBLE,

    /**
     *  Omits circular references from the output. By default, the converter
     *  will throw if it discovers a circular reference. When this option is
     *  in effect, it simply skips over the reference. This is useful when
     *  converting objects managed by an ORM, where you normally don't care
     *  about backlinks.
     *
     *  @since 1.1.14
     */
    SKIP_CIRCULAR_REFERENCES,

    /**
     *  Sequences (arrays, lists, sets) will name their elements according to
     *  the parent element, with any trailing "s" removed. For example, if the
     *  parent is named "products", each child will be named "product", rather
     *  than the default "data". If the parent is named "foo", each child will
     *  also be named "foo" (since there's no "s" to remove).
     */
    SEQUENCE_NAMED_BY_PARENT,

    /**
     *  Will add an <code>index</code> attribute to the child elements of
     *  sequences (arrays, lists, sets); the value of this attribute is the
     *  element's position in the sequence (numbered from 0). This index is
     *  not terribly useful, so is no longer default behavior.
     */
    USE_INDEX_ATTR,

    /**
     *  Will add a <code>type</code> attribute to each element; see package
     *  docs for more details.
     *  <p>
     *  <em>This option implies {@link #XSD_FORMAT}</em>.
     */
    USE_TYPE_ATTR,

    /**
     *  Outputs values using formats defined by XML Schema, rather than Java's
     *  <code>String.valueOf()</code> method. Note that these formats are not
     *  flagged in the element, so sender and receiver will have to agree on
     *  the format.
     */
    XSD_FORMAT
}
