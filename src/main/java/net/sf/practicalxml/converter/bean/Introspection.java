// Copyright 2008-2014 severally by the contributors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package net.sf.practicalxml.converter.bean;


/**
 *  @deprecated This class is now contained in the KDGCommons library; all future
 *              enhancements or bugfixes will be made to that version. This version
 *              is a thin wrapper over that version, allowing the local variant of
 *              <code>IntrospectionCache</code> (also deprecated) to maintin its
 *              segmature. Both will be removed in release 1.2.
 */
@Deprecated
public class Introspection
extends net.sf.kdgcommons.bean.Introspection
{
    public Introspection(Class<?> klass)
    {
        super(klass);    
    }
}
