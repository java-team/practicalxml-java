// Copyright 2008-2014 severally by the contributors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package net.sf.practicalxml.converter.internal;

import org.w3c.dom.Element;

import net.sf.practicalxml.DomUtil;
import net.sf.practicalxml.converter.AbstractConversionTestCase;
import net.sf.practicalxml.converter.ConversionConstants;

public class TestConversionUtils
extends AbstractConversionTestCase
{
    public TestConversionUtils(String testName)
    {
        super(testName);
    }


//----------------------------------------------------------------------------
//  Test Cases
//----------------------------------------------------------------------------

    public void testSetAndGetAttributeWithNamespace() throws Exception
    {
        final String namespace = "urn:x-foo";
        final String attrName = "foo";
        final String attrValue = "bar";

        Element elem = DomUtil.newDocument("root", namespace);
        ConversionUtils.setAttribute(elem, attrName, attrValue);

        assertEquals("DOM retrieval",
                     attrValue,
                     elem.getAttributeNS(ConversionConstants.NS_CONVERSION, attrName));

        assertEquals("util retrieval",
                     attrValue,
                     ConversionUtils.getAttribute(elem, attrName));
    }


    public void testSetAndGetAttributeWithoutNamespace() throws Exception
    {
        final String attrName = "foo";
        final String attrValue = "bar";

        Element elem = DomUtil.newDocument("root");
        ConversionUtils.setAttribute(elem, attrName, attrValue);

        assertEquals("DOM retrieval",
                     attrValue,
                     elem.getAttributeNS(null, attrName));

        assertEquals("util retrieval",
                     attrValue,
                     ConversionUtils.getAttribute(elem, attrName));
    }
}
